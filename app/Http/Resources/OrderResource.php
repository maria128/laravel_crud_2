<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $orderItems = [];

        foreach ($this->orderItem as $orderItem) {
            $array = [
                'productName' => $orderItem->products->name,
                'quantity' => $orderItem->quantity,
                'productPrice' => $orderItem->price / 100 . '$',
                'productDiscount' => $orderItem->discount  . '%',
                'productSum' => $orderItem->getSum() . '$',
            ];
            \array_push($orderItems, $array);
        }

        return [
            'orderId' => $this->id,
            'orderDate' => $this->date,
            'orderSum' => $this->getSum() . '$',
            'orderItems' => [$orderItems],
            'buyer' => [
                'buyerFullName' => $this->buyers->getFullName(),
                'buyerAddress' => $this->buyers->buyerAddress(),
                'buyerPhone' => $this->buyers->phone,
            ]
        ];
    }
}