<?php
/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entity\Order;
use \App\Entity\OrderItem;
use \App\Product;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'product' =>  Product::query()->inRandomOrder()->first()->id,
        'order' =>  Order::query()->inRandomOrder()->first()->id,
        'price' =>  $faker->numberBetween(100, 10000),
        'quantity' =>  $faker->numberBetween(1, 100),
        'discount' =>  $faker->randomFloat(2,1, 100),
    ];
});
