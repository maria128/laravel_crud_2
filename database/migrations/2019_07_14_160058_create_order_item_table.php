<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product');
            $table->unsignedBigInteger('order');
            $table->integer('quantity');
            $table->integer('price');
            $table->decimal('discount');
            $table->timestamps();

            $table
                ->foreign('product')
                ->references('id')
                ->on('my_products');

            $table
                ->foreign('order')
                ->references('id')
                ->on('order');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_item');
    }
}
