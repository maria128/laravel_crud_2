<?php

namespace App\Http\Controllers;

use App\Entity\Order;
use App\Entity\OrderItem;
use App\Http\Resources\OrderResource;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class OrderController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Input::all();
        $order = new Order();
        $order->date = Carbon::now()->format('Y-m-d H:i:s');

        $order->buyer = $data['buyerId'];
        $order->save();
        foreach ($data['orderItems'] as $item) {
            $product = Product::find($item['productId']);

            if (!$product){
                return new Response([
                    'result' => 'fail',
                    'message' => 'Product not founded'
                ]);
            }
            $orderItem = new OrderItem();
            $orderItem->order = $order->id;

            $orderItem->product = $item['productId'];

            $orderItem->quantity = $item['productQty'];

            $orderItem->price = $product->price;

            $orderItem->discount = str_replace("%","",$item['productDiscount']);

            $orderItem->save();

        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);

        if (!$order) return new Response([
            'result' => 'fail',
            'message' => 'order not found'
        ]);

       return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);

        if (!$order) return new Response([
            'result' => 'fail',
            'message' => 'order not found'
        ]);

        $data = Input::all();

        $order->orderItem()->delete();
        foreach ($data['orderItems'] as $item) {
            $product = Product::find($item['productId']);
            if (!$product){
                return new Response([
                    'result' => 'fail',
                    'message' => 'Product not founded'
                ]);
            }
            $orderItem = new OrderItem();
            $orderItem->order = $order->id;
            $orderItem->product = $item['productId'];
            $orderItem->quantity = $item['productQty'];
            $orderItem->price = $product->price;
            $orderItem->discount = str_replace("%","",$item['productDiscount']);
            $orderItem->save();

        }
        return new Response(new OrderResource($order));
    }
}
